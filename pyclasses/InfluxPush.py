# coding=utf-8
__version__ = '190903'

import logging
import influxdb

# --- version 190903 ---
class InfluxPush :

    # -------------------------- Init --------------------------
    def __init__(self, params=None):
        self.myInfluxClient   = None
        self.myInfluxClientDF = None
        if params is None :
            params = dict()
        self.myIP           = params.get('ip')          or '127.0.0.1'
        self.myPort         = params.get('port')        or int(8086)
        self.myUsername     = params.get('username')    or 'username'
        self.myPassw        = params.get('password')    or 'password'
        self.myDefaultDB    = params.get('dbdefault')   or 'defaultdb'
        self.myTimeout      = params.get('timeout')     or int(10)

    # -------------------------- Quick insert  --------------------------
    def insertInIDB(self, dbnameoverride=None, liste_objets=None):
        """ Insert 1 or more records into Influx. In case of error, the push will be forgotten. If influx not connected, the connection will be done.
        :param dbnameoverride: if different than the one initialized.
        :param liste_objets: List of dict for influx : timestamp, measurements, tags, fields
        :return: True if sucess, False if problem
        """
        retour = False
        if liste_objets is not None :
            try :
                if self.myInfluxClient is None :
                    self.myInfluxClient = influxdb.InfluxDBClient(host=self.myIP, port=self.myPort, username=self.myUsername, password=self.myPassw, database=self.myDefaultDB, timeout=self.myTimeout)
                self.myInfluxClient.write_points(database=dbnameoverride or self.myDefaultDB, points=liste_objets)
                retour = True
            except Exception as e :
                logging.error("Exception Insert InfluxDB : %s" % str(e))
        return retour

    # -------------------------- Query --------------------------
    def queryIDB(self, query=""):
        # http://influxdb-python.readthedocs.io/en/latest/api-documentation.html#influxdb.InfluxDBClient.query
        retour = None
        if query != "" is not None :
            try :
                if self.myInfluxClient is None :
                    self.myInfluxClient = influxdb.InfluxDBClient(host=self.myIP, port=self.myPort, username=self.myUsername, password=self.myPassw, database=self.myDefaultDB, timeout=self.myTimeout)
                retour = self.myInfluxClient.query(query=query)
            except Exception as e :
                logging.error("Exception Query InfluxDB : %s" % str(e))
        return retour

    # -------------------------- Dataframe as output --------------------------
    def queryIDB_df(self, query=""):
        retour = None
        if query != "" is not None :
            try :
                if self.myInfluxClientDF is None :
                    self.myInfluxClientDF = influxdb.DataFrameClient(host=self.myIP, port=self.myPort, username=self.myUsername, password=self.myPassw, database=self.myDefaultDB, timeout=self.myTimeout)
                retour = self.myInfluxClientDF.query(query=query)
            except Exception as e :
                logging.error("Execption QueryDF InfluxDB : %s" % str(e))
        return retour

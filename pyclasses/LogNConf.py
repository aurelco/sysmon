# coding=utf-8
__version__ = '200428'

import toml  # pip3 install toml
import os
import sys
import logging.handlers
import getpass
import pickle
from cryptography.fernet import Fernet  # pip3 install cryptography
import base64
import json
import pathlib
import collections
import signal
import time
import cherrypy  # pip3 install cherrypy

class LogNConf :
    """ Class with static fonction to configure and initialize logging & config.
    Logging : choose level, log into console or rotating file, a list of directories for log file, format for log.
    Config : with .toml format, allow several things
        - load default config files from a shared github, easier for project
        - some fields can be prompted live
        - the config can be saved in a pickle crypted file
        - the config can be manually edited in a .local file, toml is human readable
        - config will be loaded with deep mearge : default -> pickle -> .local
    """
    log                  = None
    conf                 = dict()
    lets_go_on           = True
    config_updated       = False
    signal_received      = False
    log_PathFile         = pathlib.Path()
    conf_PathToml        = pathlib.Path()
    conf_PathToml_local  = pathlib.Path()
    conf_PathPickle      = pathlib.Path()
    _conf_in_pickle      = dict()
    _ask_save_pickle     = False
    _prompted_value      = False
    _loaded_from_pickle  = False

    @staticmethod
    def initLogging(level=logging.INFO, log_in_console=True, log_in_file=True, logfilename="" , logfilepath=("../log" , "./log", "../logs", "./logs", ".") ,
                    formatstring="P-T %(process)5d-%(thread)5d | %(name)5.5s | %(levelname)5.5s | %(asctime)s | %(message)s",
                    proposeSavePickle=False) :
        """ Deal with logging initialization. Prevent loggers from libs to be noisy (they will stay at warning level)
        :param level: Level from logging. logging.DEBUG logging.INFO ...
        :param log_in_console: True or False
        :param log_in_file: True or False
        :param logfilename: name of log file or None for automatic
        :param logfilepath: tuple list of string to search (".")
        :param formatstring: String of formatting to be used for logs
        :param proposeSavePickle: a pickle can be generated from the config for automatic and cyphered config save
        :return: the logger initialized, event if logging.__ can be used.
        """
        try :
            # https://docs.python.org/3/library/logging.html#logging.Formatter
            LogNConf._ask_save_pickle = proposeSavePickle
            log_forma   = logging.Formatter(fmt=formatstring) # %(levelno)2d
            LogNConf.log      = logging.getLogger()
            LogNConf.log.setLevel(level)

            # -- Log Console
            if log_in_console :
                consolehandler = logging.StreamHandler()
                consolehandler.setLevel(level)
                consolehandler.setFormatter(fmt=log_forma)
                LogNConf.log.addHandler(consolehandler)
            # -- Log Rotating File
            if 'win32' in sys.platform :
                log_in_file = False  # On windows, rotating files in multithread has a problem of concurrent access
            if log_in_file :
                fpath = pathlib.Path(".")
                for ifp in logfilepath :
                    if pathlib.Path(ifp).exists() :
                        fpath = pathlib.Path(ifp)
                        break

                appli_name = logfilename
                if not logfilename or len(logfilename)<1 :
                    appli_abspath = pathlib.Path(os.path.abspath(sys.argv[0]))
                    appli_name    = "{:s}.log".format(appli_abspath.stem)

                LogNConf.log_PathFile = fpath / appli_name
                LogFileName = LogNConf.log_PathFile.as_posix()
                filehandler = logging.handlers.RotatingFileHandler(filename=LogFileName, mode='w', maxBytes=100000, backupCount=1)
                filehandler.setLevel(level)
                filehandler.setFormatter(fmt=log_forma)
                LogNConf.log.addHandler(filehandler)
            # -- Mute
            LogNConf.muteLoggers()
            # -- Init log
            logging.info("Starting")
        except Exception as LEX :
            logging.critical("Exception with logging, exiting : {:s}".format(str(LEX)))
            sys.exit(-1)

        return LogNConf.log


    # Return the number of fields that has been prompted in the console.
    @staticmethod
    def readConfWithConsolePromptAndPickleOnDisk(conffilepath=("." , "./config", "../config" , "../../config") , conffilename=None, ForceReloadFromFile=True) :
        theconf          = LogNConf.getConf(conffilepath, conffilename, ForceReloadFromFile)
        nbFieldsPrompted = LogNConf._prompt_for_values(propose_pickle=True)
        return nbFieldsPrompted

    @staticmethod
    def getConf(conffilepath=("." , "./config", "../config" , "../../config") , conffilename=None, ForceReloadFromFile=False) :
        """ Get config from several sources. what is done :
        1/ take filename provided or take the one of the application (and add .toml to it)
        2/ find the 1st matching path in the list provided in param.
        3/ if .toml config file doesn't exist, create an empty one
        4/ create conf object from reading toml file
        5/ if .pickle file exists, load and deep merge it
        6/ if .local.extension (.toml normally) file exists, load and deep merge
        :param conffilepath: list of path to search for the file, 1 found stop the search
        :param conffilename: filename of the config name, name of application will be taken by default.
        :param ForceReloadFromFile: If conf already in memory, don't reload files, just deliver the dict from RAM.
        :return: dict object of the conf (dict() in case of problem)
        """
        if len(LogNConf.conf) > 0 and not ForceReloadFromFile :
            pass  # do nothing, just deliver from RAM
        else :
            LogNConf.conf = dict()
            try :
                appli_name = conffilename
                if not conffilename or len(conffilename)<1 :
                    appli_abspath = pathlib.Path(os.path.abspath(sys.argv[0]))
                    appli_name    = "{:s}.toml".format(appli_abspath.stem)

                conf_path = pathlib.Path(".") / appli_name
                for ifp in conffilepath :
                    if (pathlib.Path(ifp) / appli_name).exists() :
                        conf_path = pathlib.Path(ifp) / appli_name
                        break
                if not conf_path.exists() :
                    raise Exception('Config file cannot be found {:s}'.format(conf_path.as_posix()))
                else :
                    LogNConf.conf_PathToml       = conf_path
                    f_path               = LogNConf.conf_PathToml.parent
                    f_stem               = LogNConf.conf_PathToml.stem    # 'configfile' 'configfile.local'
                    f_suff               = LogNConf.conf_PathToml.suffix  # '.toml'
                    LogNConf.conf_PathToml_local = f_path / "{:s}.local{:s}".format(f_stem, f_suff)
                    LogNConf.conf_PathPickle     = f_path / "{:s}.pickle".format(f_stem)
                    LogNConf.conf        = LogNConf._readFromFile()
                    # logging.debug("Config loaded = {:s}".format(str(LogNConf.conf.keys())))
            except Exception as CEX :
                logging.critical("Exception config, exiting : {:s}".format(str(CEX)))
                sys.exit(-2)

        return LogNConf.conf

    @staticmethod
    def set_signals():
        signal.signal(signal.SIGTERM,  LogNConf._stop_on_signal)     # termination signal. The default behavior is to terminate the process, but it also can be caught or ignored
        signal.signal(signal.SIGINT,   LogNConf._stop_on_signal)     # interrupt signal. The terminal sends it to the foreground process when the user presses ctrl-c
        # https://docs.python.org/3/library/sys.html#sys.platform
        if 'win32' in sys.platform :
            signal.signal(signal.SIGBREAK, LogNConf._stop_on_signal)   # not on linux
        elif 'linux' in sys.platform :
            signal.signal(signal.SIGQUIT,  LogNConf._stop_on_signal)   # not on windows, it is the dump core signal

    @staticmethod
    def dumpPaths():
        # return str("Log={:s} ; ConfPickle={:s} ; ConfLocal={:s}".format(LogNConf.log_PathFile.absolute().as_posix(),
        # LogNConf.conf_PathToml.absolute().as_posix(), LogNConf.conf_PathPickle.absolute().as_posix(), LogNConf.conf_PathToml_local.absolute().as_posix()))
        return { 'log' : '{:s}'.format(LogNConf.log_PathFile.absolute().as_posix()) ,
                 'conf_toml' : '{:s}'.format(LogNConf.conf_PathToml.absolute().as_posix()) ,
                 'conf_pickle' : '{:s}'.format(LogNConf.conf_PathPickle.absolute().as_posix()) ,
                 'conf_local' : '{:s}'.format(LogNConf.conf_PathToml_local.absolute().as_posix()) }

    @staticmethod
    def sleep_with_signal_handling(sleeptime=60):
        checkinterval = 2
        nbsleep       = int(sleeptime / checkinterval)
        sleeprest     = 0
        if nbsleep<1 :
            nbsleep = 0
            sleeprest = sleeptime
        else :
            sleeprest = sleeptime%checkinterval
        while nbsleep > 0 and LogNConf.lets_go_on :
            nbsleep -= 1
            time.sleep(checkinterval)
        if sleeprest > 0.0 and LogNConf.lets_go_on :
            time.sleep(sleeprest)
        return True if LogNConf.lets_go_on else False

    # Called in initLogging, but can be called again later if needed
    @staticmethod
    def muteLoggers(liste_loggers=None):
        # -- Removing some loggers
        loggers_to_limit = [ 'mailparser.' , 'pil.image' , 'urllib3.connectionpool' , 'kafka' , 'cherrypy.' , 'pyftpdlib' , 'chardet' ]
        if liste_loggers :
            if len(liste_loggers) > 0 :
                loggers_to_limit = loggers_to_limit + liste_loggers
        for log_mgr_key in LogNConf.log.manager.loggerDict.keys():
            if any( thepattern in str(log_mgr_key).lower() for thepattern in loggers_to_limit ) :
                logging.getLogger(log_mgr_key).setLevel(logging.WARNING)

    @staticmethod
    def _stop_on_signal(signum=0, frame=None) :
        logging.critical("Signal received")
        LogNConf.lets_go_on = False
        LogNConf.signal_received = True

    @staticmethod
    def _dict_deep_merge(dct, merge_dct):
        """ Recursive dict merge. Inspired by :meth:``dict.update()``, instead of updating only top-level keys, dict_merge recurses down into dicts nested
        to an arbitrary depth, updating keys. The ``merge_dct`` is merged into ``dct``. When a field exists, it will be UPDATED.
        :param dct: dict onto which the merge is executed
        :param merge_dct: dct merged into dct
        :return: None
        """
        for k, v in merge_dct.items():
            if (k in dct) and isinstance(dct[k], dict) and isinstance(merge_dct[k], collections.abc.Mapping) :
                LogNConf._dict_deep_merge(dct[k], merge_dct[k])
            else:
                dct[k] = merge_dct[k]

    @staticmethod
    def _prompt_for_values(propose_pickle=False):
        LogNConf._prompted_value = False
        NbFieldsPrompted = 0
        # reccursive function

        def recur_prompt(dict_field, name_str) :
            nonlocal NbFieldsPrompted
            thekeys = dict_field.keys()
            #for thekey in sorted(thekeys) :
            for thekey in thekeys :
                if not LogNConf.lets_go_on :
                    break
                if type(dict_field[thekey]) is dict :
                    if len(name_str)>0 :
                        recur_prompt(dict_field=dict_field[thekey], name_str="{:s}[{:s}]".format(name_str, thekey))
                    else :
                        recur_prompt(dict_field=dict_field[thekey], name_str="[{:s}]".format(thekey))
                elif type(dict_field[thekey]) is str :
                    if dict_field[thekey].startswith("TO_PROMPT") :
                        if len(name_str) > 0:
                            name_str2 = "{:s}[{:s}]".format(name_str, thekey)
                        else :
                            name_str2 = "[{:s}]".format(thekey)

                        fTmp, fType, fDefault = dict_field[thekey].split(";")
                        if fType not in [ 'int' , 'float', 'str' ] :
                            fType = 'str'
                        fQuestion = "\nConfig : Enter value for {:s} ({:s} , default={:s}) : ".format(name_str2, fType, str(fDefault))

                        fInput = ''
                        try :
                            if dict_field[thekey].startswith("TO_PROMPT_SECRET") :
                                fInput = getpass.getpass(fQuestion)
                            else :
                                fInput = input(fQuestion)
                        except :
                            pass
                        fInput2 = fDefault
                        if len(fInput) > 0 :
                            fInput2 = fInput
                        if fType == 'int' :
                            fInput2 = int(fInput2)
                        elif fType == 'float' :
                            fInput2 = float(fInput2)
                        else :
                            fInput2 = str(fInput2)
                        dict_field[thekey] = fInput2
                        NbFieldsPrompted += 1
                        LogNConf._prompted_value = True

        if LogNConf.lets_go_on :
            try :
                recur_prompt(LogNConf.conf, "")
            except Exception as recur:
                logging.critical("Exception while prompting for config : {:s}".format(str(recur)))
                sys.exit(-1)

        if LogNConf.lets_go_on and propose_pickle and ( not LogNConf.conf_PathPickle.exists() or LogNConf._prompted_value ) :
            # ok for pickle + no pickle file exists or new values have been entered, so to refresh
            qPic = 'y'
            try :
                qPic = input("\nDo you want to save locally (y/n, default=N) ?")
            except :
                pass
            if qPic.lower() in ['y', 'yes'] :
                try:
                    with LogNConf.conf_PathPickle.open(mode="wb") as fp :
                        # using json between toml & pickle : pickling directly from toml generated dict has some issue
                        chaine = pickle.dumps(json.dumps(LogNConf.conf), protocol=pickle.HIGHEST_PROTOCOL)
                        fp.write(LogNConf._getFernet().encrypt(chaine))

                except Exception as epick:
                    logging.error("Cannot write pickle file : {:s}".format(str(epick)))

        return NbFieldsPrompted

    @staticmethod
    def _readFromFile():
        retour      = dict()
        f_path      = LogNConf.conf_PathToml.parent
        f_stem      = LogNConf.conf_PathToml.stem    # 'configfile' 'configfile.local'
        f_suff      = LogNConf.conf_PathToml.suffix  # '.toml'

        # --- if file not exist, creating an empty one
        if not LogNConf.conf_PathToml.exists() :
            # ---- Creation of a TOML file if it doesn't exist
            tmpTexte =  '# TOML : https://github.com/toml-lang/toml  and  https://github.com/avakar/pytoml\n'
            tmpTexte += 'title = "{:s} default created"\n'.format(f_stem)
            try :
                LogNConf.conf_PathToml.write_text(data=tmpTexte, encoding='utf-8', errors='backslashreplace')
            except Exception as etoml:
                logging.critical("Cannot write default Toml Config file {:s} : {:s}\nExiting.".format(LogNConf.conf_PathToml.as_posix(), str(etoml)))
                sys.exit(-1)

        # --- reading from .toml
        if LogNConf.conf_PathToml.exists() :
            try :
                toml_text = LogNConf.conf_PathToml.read_text(encoding='utf-8', errors='backslashreplace')
                retour    = toml.loads(toml_text)
                # logging.debug("config loaded from {:s}".format(LogNConf.conf_PathToml.absolute().as_posix()))
            except Exception as etoml:
                logging.critical("Cannot read Toml Config file {:s} : {:s}\nExiting.".format(LogNConf.conf_PathToml.as_posix(), str(etoml)))
                sys.exit(-1)

        # Si le pickle se loade a la fin, il va effacer les existants il faudrait que le pickle ne contiennent que les champs promptes
        # --- reading + merge from .pickle
        if LogNConf.conf_PathPickle.exists() :
            try :
                toml_text = LogNConf.conf_PathPickle.read_text(encoding='utf-8', errors='backslashreplace')
                conf2     = json.loads(pickle.loads(LogNConf._getFernet().decrypt(toml_text.encode())))
                LogNConf._dict_deep_merge(retour, conf2)
                # logging.debug("config loaded and merged from {:s}".format(LogNConf.conf_PathPickle.absolute().as_posix()))
            except Exception as etoml:
                logging.critical("Cannot read or decypher Pickle Config file {:s} : {:s}\nExiting.".format(LogNConf.conf_PathPickle.absolute().as_posix(), str(etoml)))
                sys.exit(-1)

        # --- reading + merge from .local.toml : le local ecrasera le reste
        if LogNConf.conf_PathToml_local.exists() :
            try :
                toml_text = LogNConf.conf_PathToml_local.read_text(encoding='utf-8', errors='backslashreplace')
                conf2     = toml.loads(toml_text)
                LogNConf._dict_deep_merge(retour, conf2)
                # logging.debug("config loaded and merged from {:s}".format(LogNConf.conf_PathToml_local.absolute().as_posix()))
            except Exception as etoml:
                logging.critical("Cannot read Local Config file {:s} : {:s}\nExiting.".format(LogNConf.conf_PathToml_local.absolute().as_posix(), str(etoml)))
                sys.exit(-1)

        # --- In case some values should be "PROMPTED"
        LogNConf.conf = retour
        # By default, no prompt --> TO BE PLANNED IN THE APP TO ASK FOR QUESTIONS IN CONSOLE.
        # Because it may block while launch without console.
        # LogNConf._prompt_for_values(propose_pickle=LogNConf._ask_save_pickle)
        return LogNConf.conf

    @staticmethod
    def _getFernet():
        chaine_seed = LogNConf.conf_PathPickle.name
        tmpK2 = int(32/len(chaine_seed) + 2)
        tmpK3 = chaine_seed * tmpK2
        tmpK4 = tmpK3[:32]
        retour = Fernet(base64.b64encode(tmpK4.encode()))
        return retour

    # ---------------------------------- WEBSERVER : WEB ERROR MANAGEMENT ----------------------------------
    @staticmethod
    def error_page(status, message, traceback, version):
        logging.warning("HTTP ERR = %s | %s | %s | %s" % (str(status), str(message), str(version), str(traceback)[:5]))
        return "<html><body>Error %s</body></html>" % str(status)
    @staticmethod
    def handle_error():
        cherrypy.response.status = 500
        cherrypy.response.body = b"Error 500"
    # ---------------------------------- WEBSERVER : CREDENTIALS MANAGEMENT ----------------------------------
    theusers = dict()
    @staticmethod
    def init_users(dico):
        """Read users from a dict and implement the class variable.
        :param dico: dict() of login : pwd ALREADY HASHED
        :return: None
        """
        LogNConf.theusers = dict(dico)
    @staticmethod
    def validate_password(realm='localhost', username='', password=''):
        return False

    # ---------------------------------- WEBSERVER : CHERRY PY SETUP & LAUNCH ----------------------------------
    @staticmethod
    def StopCherrypyServer():
        try :
            cherrypy.engine.stop()
            cherrypy.engine.exit()
        except :
            pass
    @staticmethod
    def StartCherrypyServer(serving_port=10000):
        # ---------------------------- CHERRYPY CONFIG SERVER ----------------------------
        server_config = {   # http://docs.cherrypy.org/en/latest/advanced.html#securing-your-server
            'server.socket_host'            : '0.0.0.0',
            'server.socket_port'            : serving_port,
            'server.socket_queue_size'      : 10,        # The "backlog" argument to socket.listen(); specifies the maximum number of queued connections (default 5).
            'server.socket_timeout'         : 10,        # The timeout in seconds for accepted connections (default 10).
            'server.accepted_queue_size'    : 60,        # The maximum number of requests which will be queued up before the server refuses to accept it (default -1, meaning no limit).
            'server.thread_pool'            : 10,        # The number of worker threads to start up in the pool.
            'server.thread_pool_max'        : 40,        # The maximum size of the worker-thread pool. Use -1 to indicate no limit.

            'error_page.default'     : LogNConf.error_page,  # en cas de parametres incorrects sur une URL/fonction, on finit en 404
            'request.error_response' : LogNConf.handle_error,

            'log.screen' : False, 'log.access_file': '' , 'log.error_file': '',
            'engine.autoreload.on' : False,  # Sinon le server se relance des qu'un fichier py est modifie...
        }
        cherrypy.config.update(server_config)

        # ---------------------------- CHERRYPY CONFIG APPLICATIONS ----------------------------
        WebAppli_root_url = "/"
        WebAppli_config = {
            # # --- Static files : Serving 1 single static file = the icon of the website
            # '/favicon.ico' : {
            # 	'tools.staticfile.on'       : True,
            # 	'tools.staticfile.filename' : Path().cwd().joinpath("www").joinpath("images").joinpath("favicon.gif").as_posix()
            # 	},
            # --- Service website and application
            '/': {
                # ----- Authentication -----
                'tools.auth_basic.on'            : False,
                'tools.auth_basic.realm'         : 'localhost',
                # 'tools.auth_basic.checkpassword' : AppliWeb.validate_password,
                # ----- Static files : Serving A WHOLE DIRECTORY -----
                'tools.staticdir.on'     : False,
                # 'tools.staticdir.dir'    : Path().cwd().joinpath("www").as_posix(),
                # 'tools.staticdir.index'  : "index.html",
                # ----- Sessions -----
                'tools.sessions.on'       : False,
                'tools.sessions.secure'   : True,
                'tools.sessions.httponly' : False,   # If False (the default) the cookie 'httponly' value will not be set. If True, the cookie 'httponly' value will be set (to 1).
                }
            }

        # ---------------------------- CHERRYPY LAUNCH ----------------------------
        cherrypy.tree.mount(LogNConf(), WebAppli_root_url, WebAppli_config)  # http://IP:port/ = nothing ; http://IP:port/uiot/ = 404 error
        # cherrypy.engine.signals.subscribe()  # avec ca le ctrl-c est trappe par cherrypy, plus dans le reste de l'appli
        cherrypy.engine.start() #  cherrypy.engine.block()  # On ne bloque pas le thread principal, mais il faut eteindre cherrypy proprement en quittant
        logging.info("Service config on web : http://127.0.0.1:{:d}/menuconfig".format(serving_port))

    # ---------------------------------- WEB APPLICATION ITSELF ----------------------------------
    def __init__(self):
        pass

    @cherrypy.expose()
    # @cherrypy.tools.json_in()
    # @cherrypy.tools.json_out()
    def menuconfig(self, _=""):
        appli_name = "Application"
        config_json = json.dumps(LogNConf.conf, indent="   ", separators=(", ", " : ") , sort_keys=True)
        config_toml = toml.dumps(LogNConf.conf)
        html  = "<!DOCTYPE html><head><title>{:s} - Menu Config</title></head>".format("appli_name")
        html += '<body><form action="/menuconfigform" method="post" target="_blank">'
        html += '<br><p>JSON Config <a href="/showconfigJson" target="_blank">link2json</a></p>'
        html += '<textarea name="texte" rows="10" cols="70">{:s}</textarea>'.format(config_json)
        html += '<br><input type="submit" name="SubmitJson" value="Update from JSON">'
        html += '</form><form action="/menuconfigform" method="post" target="_blank">'
        html += '<br><p>TOML Config <a href="/showconfigToml" target="_blank">link2toml</a></p>'
        html += '<textarea name="texte" rows="10" cols="70">{:s}</textarea>'.format(config_toml)
        html += '<br><input type="submit" name="SubmitToml" value="Update from TOML">'
        html += '</form><br><p>{:s}</p></body></html>'.format(str(LogNConf.dumpPaths()))
        return html

    @cherrypy.expose()
    def showconfigToml(self, _=""):
        config_toml = "Cannot extract Toml config"
        try :
            config_toml = toml.dumps(LogNConf.conf)
            cherrypy.response.headers['Content-Type'] = 'text/plain'
        except :
            pass
        return config_toml

    @cherrypy.expose()
    # @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def showconfigJson(self, _=""):
        return LogNConf.conf

    @cherrypy.expose()
    def menuconfigform(self, _="", SubmitJson="", SubmitToml="", texte=""):
        retour = "Nothing done"
        if len(SubmitJson)>0 :
            newConf = dict()
            try :
                newConf = json.loads(s=texte)
            except Exception as NewC :
                logging.error("Cant update conf from json : {:s}".format(str(NewC)))
                retour = "Not updated : {:s}".format(str(NewC))
            if len(newConf) > 0 :
                LogNConf._dict_deep_merge(LogNConf.conf, newConf)
                LogNConf.config_updated = True
                LogNConf.lets_go_on = False
                retour = "Conf updated (merge)"

        elif len(SubmitToml)>0 :
            newConf = dict()
            try :
                newConf = toml.loads(texte)
            except Exception as NewC :
                logging.error("Cant update conf from json : {:s}".format(str(NewC)))
                retour = "Not updated : {:s}".format(str(NewC))
            if len(newConf) > 0 :
                LogNConf._dict_deep_merge(LogNConf.conf, newConf)
                LogNConf.config_updated = True
                LogNConf.lets_go_on = False
                retour = "Conf updated (merge)"

        cherrypy.response.headers['Content-Type'] = 'text/plain'
        return retour

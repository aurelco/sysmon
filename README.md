## Customizable System Monitor 

#### What is it ?
Python agent collecting System metrics and/or Processes load and/or Nvidia GPU metrics.
Collection is sent into influx/mqtt.
Based on different libraries to collect system/gpu metrics on different OS. 

Differences from tools like glances : customizable measures (same bottom lib is leveraged), control on the data model, mqtt connection working ssl.  
  

#### How to use for Linux & Windows ?
On each system to monitor :
- Install Python 3.6+
- Copy Python agents in a directory
- Modify config files for Python agent (.toml files)
- Launch the agents in a shell for 1st attempt in console.
- Use .sh / .bat file for detached execution. 



### Install python x64 >= 3.5

To check version :
```
$ python3 -V
```
Install/upgrade if needed. psutil and the agent are using specific functions from 3.5+ version.

##### Install pip3
pip3 is the package manager for python (here the 3.x version).
On Debian based distribs, can be done with apt-get :
```
$ sudo apt-get install python3-pip
``` 

##### Install or Upgrade Python packages required for the agent
To deal with system metrics, influxdb connection, toml configuration files, timezone calculation :
```
$ pip3 install psutil influxdb toml pytz cryptography gputil paho-mqtt
or
$ pip3 --proxy http://... install psutil influxdb toml pytz cryptography gputil paho-mqtt
```

Launch python3 interpreter and check psutil version.

It has to be **>= 5.4.x** :

```
$ python3
>>> import psutil
>>> psutil.__version__
>>> '5.4.5'
```

If needed, install or upgrade the package. Careful with **sudo** usage, try without first.

```
Install the psutil package if not already on the machine :
$ pip3 install psutil pytz gputil  toml cryptography

Install python sources 
$ sudo apt-get install python3-dev

Force an upgrade
$ sudo -H pip3 install --upgrade --ignore-installed psutil
Option : -H to deal with sudo/root/user access to directories
Option : --ignore-installed if previous lib version cannot be uninstalled.
```

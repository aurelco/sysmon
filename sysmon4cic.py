# coding=utf-8
__version__ = '200717'
import sys
import psutil       # https://pypi.org/project/psutil/ http://psutil.readthedocs.io/en/latest/
import time
import datetime
import pytz
import copy
import re
import logging.handlers
import pyclasses.LogNConf
import pyclasses.InfluxPush
import GPUtil  # pip install gputil
import ssl
import paho.mqtt.client  # install paho-mqtt
import json


# ------ MQTT Client ------
class myMqttClient :
    connected_clients = list()

    def __init__(self, mqttconfig):
        self._mqttconfig = mqttconfig
        self.mqttclient = None

    def init_connect_and_start(self) :
        retour = True
        try :
            self.mqttclient = paho.mqtt.client.Client(client_id=self._mqttconfig['client_id'], clean_session=True, protocol=paho.mqtt.client.MQTTv311, transport="tcp")
            if self._mqttconfig['ssl'] > 0 :
                SSLcontext = ssl.create_default_context()
                self.mqttclient.tls_set_context(context=SSLcontext)
            self.mqttclient.username_pw_set(username=self._mqttconfig['username'], password=self._mqttconfig['userpass'])

            self.mqttclient.on_connect    = myMqttClient.mqtt_on_connect
            self.mqttclient.on_disconnect = myMqttClient.mqtt_on_disconnect
            self.mqttclient.on_message    = myMqttClient.mqtt_on_message

            self.mqttclient.connect(host=self._mqttconfig['host'], port=self._mqttconfig['port'], keepalive=self._mqttconfig['keepalive'], bind_address="")
            # Starting a loop that manage the publish & subscribe in a thread  # https://pypi.org/project/paho-mqtt/
            self.mqttclient.loop_start()
            tmptime0 = time.time()
            while self.mqttclient not in myMqttClient.connected_clients :
                if time.time() - tmptime0 > 60 :
                    raise Exception('Mqtt timeout while connecting')
                time.sleep(2)
        except Exception as CEX2 :
            logging.critical("Exception while MQTT connection : {:s}".format(str(CEX2)))
            if self.mqttclient in myMqttClient.connected_clients :
                myMqttClient.connected_clients.remove(self.mqttclient)
            retour = False
        return retour

    def stop_and_disconnect(self) :
        try :
            self.mqttclient.loop_stop()
        except :
            pass
        try :
            self.mqttclient.disconnect()
        except :
            pass
        if self.mqttclient in myMqttClient.connected_clients :
            myMqttClient.connected_clients.remove(self.mqttclient)
        self.mqttclient = None

    @staticmethod
    def mqtt_on_connect(client, userdata, flags, rc):
        # global mqtt_source_topic
        try :
            logging.info("MQTT Client Connected")
            logging.debug("MQTT Client Connected : {:s} / {:s} / {:s} / {:s}".format(str(client), str(userdata), str(flags), str(rc)))
            myMqttClient.connected_clients.append(client)
            # --- Subscribe to topics ---
            # client.subscribe(topic="{:s}/#".format(mqtt_source_topic))
        except Exception as MQC :
            logging.error("EXCEPT MQTT Connection : {:s}".format(str(MQC)))

    @staticmethod
    def mqtt_on_disconnect(client, userdata, rc) :
        logging.warning("MQTT Client Disconnected")
        if client in myMqttClient.connected_clients :
            myMqttClient.connected_clients.remove(client)

    @staticmethod
    def mqtt_on_message(client, userdata, msg):
        try :
            topic   = str(msg.topic)
            payload = json.loads(bytes.decode(msg.payload))
        except Exception as E2E :
            logging.warning("Cannot extract payload : {:s}".format(str(E2E)))

# ----- Getting global system metrics -----
def getSystemMetrics(PreviousData=None, IDBTemplate=None) :
    PreviousData = dict() if not PreviousData else PreviousData
    IDBTemplate  = dict() if not IDBTemplate  else IDBTemplate
    retour4idb   = list()
    global conf
    enableCPUall   = conf.get('system').get("enableCPUall")   or False
    enableCPUCores = conf.get('system').get("enableCPUCores") or False
    enableMemory   = conf.get('system').get("enableMemory")   or False
    enableIOdisk   = conf.get('system').get("enableIOdisk")   or False
    enableIOnetw   = conf.get('system').get("enableIOnetw")   or False

    IDBTemplate['time']   = agent_timezone.fromutc(datetime.datetime.utcnow()).isoformat()   # get really the closest time
    # -------------------------------- Metrics : all together to regoup in time ------
    # -- CPU
    tmpCPU_TS = time.perf_counter()
    tmpCPUdetails = list(psutil.cpu_times(percpu=True))

    # -- RAM
    tmpMem = tmpMemSwap = None
    if enableMemory : # http://psutil.readthedocs.io/en/latest/#psutil.virtual_memory
        tmpMem     = psutil.virtual_memory() # better to get available than free (available < free)
        # answer = svmem(total=16975540224, available=12569464832, percent=26.0, used=4406075392, free=12569464832)
        tmpMemSwap = psutil.swap_memory()
        # answer = sswap(total=2097147904L, used=886620160L, free=1210527744L, percent=42.3, sin=1050411008, sout=1906720768)

    # -- IO Disks
    tmpIODTS = time.perf_counter()
    tmpIOD   = None
    if enableIOdisk :
        # On Windows it may be ncessary to issue diskperf -y command from cmd.exe first in order to enable IO counters.
        # Difference to be done between 2 timesteps
        try :
            tmpIOD = psutil.disk_io_counters(perdisk=False) # compatib avec versions anterieures, nowrap=True)
            # answer = sdiskio(read_count=170221, write_count=137388, read_bytes=4764322304, write_bytes=13844901376, read_time=93, write_time=209)
            # psutil.disk_io_counters(perdisk=True, nowrap=True)
            # answer = {'PhysicalDrive0': sdiskio(read_count=167361, write_count=106344, read_bytes=4705313280, write_bytes=3699215872, read_time=92, write_time=109), 'PhysicalDrive1': sdiskio(read_count=22, write_count=7, read_bytes=23040, write_bytes=28672, read_time=0, write_time=0), 'PhysicalDrive2': sdiskio(read_count=19, write_count=0, read_bytes=0, write_bytes=0, read_time=0, write_time=0)}
        except :
            tmpIOD = None

    # -- IO network
    tmpIONTS = time.perf_counter()
    tmpION = None
    if enableIOnetw :  # http://psutil.readthedocs.io/en/latest/#psutil.net_io_counters
        # Difference to be done between 2 timesteps
        tmpION           = psutil.net_io_counters(pernic=False) # , nowrap=True) # compatib avec versions anterieures, nowrap=True)
        # answer = snetio(bytes_sent=695487271, bytes_recv=23611628841, packets_sent=8503551, packets_recv=16736054, errin=0, errout=0, dropin=0, dropout=0)

    sum(map(lambda x: max(0.0,x), [-1, 1000000.0, 154651.564]))

    # -------------------------------- KPI calculation for output ------
    if enableCPUall or enableCPUCores :
        tmpCPUprevious     = PreviousData.get('cpus-times') or None
        tmpCPU_ts_previous = PreviousData.get('cpus-ts') or None
        go_on_cpu = True
        if tmpCPUprevious :
            delta_time    = tmpCPU_TS - tmpCPU_ts_previous
            if delta_time <= 3 :
                logging.error('Less than 3 sec between 2 cpu activity calls')
            else :
                allcpu_idle   = 0.0
                allcpu_user   = 0.0
                allcpu_system = 0.0
                allcpu_others = 0.0
                allcpu_total  = 0.0
                nbcpu = len(tmpCPUdetails)
                nbcpuWithoutHThreading = 0

                # [scputimes(user=57243.147340899995, system=29083.219629700063, idle=1447765.6736897, interrupt=1250.5196161, dpc=1521.6181539), scputimes(user=12222.459948599999, system=16983.298466600012, idle=1504885.954643, interrupt=7349.253910299999, dpc=3442.7704688999997), scputimes(user=51176.783254199996, system=24627.53466780018, idle=1458287.2703353998,     interrupt=335.090148, dpc=155.6109975), scputimes(user=7471.9018965, system=5389.740949399769, idle=1521229.6958099, interrupt=843.185405, dpc=767.0101167)]
                for iproc in range(nbcpu) :
                    # proc_total_time = sum(list(tmpCPUdetails[iproc])) - sum(list(tmpCPUprevious[iproc]))
                    proc_total_time_previous = sum(map(lambda x: max(0.0,x), list(tmpCPUprevious[iproc])))
                    proc_total_time_now      = sum(map(lambda x: max(0.0,x), list(tmpCPUdetails[iproc])))
                    proc_total_time          = proc_total_time_now - proc_total_time_previous
                    # -- With the cores in hyper-threading, the values are crazy, tested on 2 machines : it is the last n/2 cores of the list.
                    if not ( 0.5 * delta_time <= proc_total_time <= 1.5 * delta_time ) :
                        # print('Core {:d} not reliable, stopping loop on cores'.format(iproc))
                        break
                    else :
                        nbcpuWithoutHThreading += 1
                        # print('Time global for proc {:d} : {:f}'.format(iproc, proc_total_time))
                        proc_idle       = max(0.0, tmpCPUdetails[iproc].idle - tmpCPUprevious[iproc].idle)
                        proc_user       = max(0.0, tmpCPUdetails[iproc].user - tmpCPUprevious[iproc].user)
                        proc_system     = max(0.0, tmpCPUdetails[iproc].system - tmpCPUprevious[iproc].system)
                        proc_others     = max(0.0, proc_total_time - proc_idle - proc_user - proc_system)

                        proc_idle       = max(0.0, tmpCPUdetails[iproc].idle - tmpCPUprevious[iproc].idle)
                        proc_user       = max(0.0, tmpCPUdetails[iproc].user - tmpCPUprevious[iproc].user)
                        proc_system     = max(0.0, tmpCPUdetails[iproc].system - tmpCPUprevious[iproc].system)
                        proc_others     = max(0.0, proc_total_time - proc_idle - proc_user - proc_system)

                        proc_idle       = proc_idle / proc_total_time * delta_time
                        proc_user       = proc_user / proc_total_time * delta_time
                        proc_system     = proc_system / proc_total_time * delta_time
                        proc_others     = proc_others / proc_total_time * delta_time

                        allcpu_idle   += proc_idle
                        allcpu_user   += proc_user
                        allcpu_system += proc_system
                        allcpu_others += proc_others
                        allcpu_total  += proc_total_time

                        # ------ CPU par core -----
                        if enableCPUCores :
                            # proc_user_pc   = 100*proc_user/proc_total_time
                            # proc_system_pc = 100*proc_system/proc_total_time
                            # proc_others_pc = 100*proc_others/proc_total_time
                            proc_idle_pc   = max(0.0, min(100.0, 100*proc_idle/proc_total_time))
                            # print("Proc {:d} : {:2.1f} / {:2.1f} / {:2.1f} / {:2.1f} = {:2.1f}".format(iproc, proc_user_pc, proc_system_pc, proc_others_pc, proc_idle_pc, proc_user_pc+proc_system_pc+proc_others_pc+proc_idle_pc))

                            idb2 = copy.deepcopy(IDBTemplate)
                            idb2['measurement']       = 'system.cpu.cores'
                            idb2['tags']['CoreID']    = "Core-{:02d}".format(iproc+1)
                            idb2['fields']['usagePC'] = float(max(0.0, 100.0-proc_idle_pc))
                            retour4idb.append(idb2)

                if go_on_cpu :
                    if allcpu_total <= 2.0 :
                        logging.error('Less than 2 sec for all proc between 2 cpu activity calls')
                    else :
                        proc_user_pc   = min(100.0, max(0.0, 100*allcpu_user/allcpu_total))
                        proc_system_pc = min(100.0, max(0.0, 100*allcpu_system/allcpu_total))
                        proc_others_pc = min(100.0, max(0.0, 100*allcpu_others/allcpu_total))
                        proc_idle_pc   = min(100.0, max(0.0, 100*allcpu_idle/allcpu_total))
                        # print("All CPUs : {:2.1f} / {:2.1f} / {:2.1f} / {:2.1f} = {:2.1f}".format(proc_user_pc, proc_system_pc, proc_others_pc, proc_idle_pc,proc_user_pc+proc_system_pc+proc_others_pc+proc_idle_pc))

                        # ------ CPU global -----
                        if enableCPUall :
                            try :
                                idbm = copy.deepcopy(IDBTemplate)
                                idbm['measurement']      = 'system.cpu.all'
                                idbm['fields']['userPC'] = float(proc_user_pc)
                                idbm['fields']['systPC'] = float(proc_system_pc)
                                idbm['fields']['ioPC']   = float(proc_others_pc)
                                idbm['fields']['idlePC'] = float(proc_idle_pc)
                                idbm['fields']['busyTotalPC'] = float(max(0.0, 100-proc_idle_pc))
                                retour4idb.append(idbm)
                            except Exception as E5 :
                                logging.error("System : Exception : %s" % str(E5))

        PreviousData['cpus-times'] = tmpCPUdetails
        PreviousData['cpus-ts']    = tmpCPU_TS

    # ------ RAM -------
    if enableMemory :
        idb3 = copy.deepcopy(IDBTemplate)
        idb3['measurement'] = 'system.memory'
        idb3['fields']['RAMavailBy']  = int(tmpMem.available)
        idb3['fields']['RAMusedBy']   = int(tmpMem.used)
        idb3['fields']['RAMusedPC']   = float(tmpMem.percent)
        idb3['fields']['SwapAvailBy'] = int(tmpMemSwap.free)
        idb3['fields']['SwapUsedBy']  = int(tmpMemSwap.used)
        idb3['fields']['SwapUsedPC']  = float(tmpMemSwap.percent)
        retour4idb.append(idb3)

    # ------ IO DISKS + NETWORK and various ------
    if enableIOdisk or enableIOnetw :
        idb4 = copy.deepcopy(IDBTemplate)
        idb4['measurement'] = 'system.io'
        iogo = False
        tmpIODTSPrevious = PreviousData['io-disks-ts']
        tmpIODPrevious   = PreviousData['io-disks-kpi']
        if enableIOdisk and tmpIODPrevious and tmpIOD :
            dt = tmpIODTS - tmpIODTSPrevious
            idb4['fields']['DiskAllReadBps']  = float((tmpIOD.read_bytes - tmpIODPrevious.read_bytes)/dt)
            idb4['fields']['DiskAllWriteBps'] = float((tmpIOD.write_bytes - tmpIODPrevious.write_bytes)/dt)
            iogo = True
        PreviousData['io-disks-ts']  = tmpIODTS
        PreviousData['io-disks-kpi'] = tmpIOD

        tmpIONTSPrevious = PreviousData['io-netw-ts']
        tmpIONPrevious   = PreviousData['io-netw-kpi']
        if enableIOnetw and tmpIONPrevious and tmpION :
            dt = tmpIONTS - tmpIONTSPrevious
            idb4['fields']['netwAllRecvByteSec'] = float((tmpION.bytes_recv - tmpIONPrevious.bytes_recv)/dt)
            idb4['fields']['netwAllSentByteSec'] = float((tmpION.bytes_sent - tmpIONPrevious.bytes_sent)/dt)
            iogo = True
        PreviousData['io-netw-ts']  = tmpIONTS
        PreviousData['io-netw-kpi'] = tmpION
        if iogo :
            retour4idb.append(idb4)

    return retour4idb

# ----- Getting processes metrics -----
def getProcessMetrics(IDBTemplate=None) :
    IDBTemplate  = dict() if not IDBTemplate  else IDBTemplate
    retour4idb   = list()
    global conf
    thresholdcpupercent = conf.get('processes').get("thresholdcpupercent") or 3.0
    namepattern         = conf.get('processes').get("nameregex") or ""
    nbcores             = psutil.cpu_count()

    # Attention : calls to psutil are real time : values are changing to each call -> do everything in 1 call
    for p in psutil.process_iter():
        # http://psutil.readthedocs.io/en/latest/#process-class + http://psutil.readthedocs.io/en/latest/#psutil.Process.as_dict
        mesures = None
        if psutil.WINDOWS :  # windows only
            mesures = p.as_dict(attrs=['pid', 'cpu_percent', 'name', 'memory_percent', 'cpu_times', 'memory_info', 'num_threads', 'num_handles'])
        elif psutil.LINUX :
            mesures = p.as_dict(attrs=['pid', 'cpu_percent', 'name', 'memory_percent', 'cpu_times', 'memory_info', 'num_threads'])

        # TODO : instantaneous CPU value, better to replace by a delta between 2 timesteps ?
        tmpCPUpc   = float(mesures.get("cpu_percent") or -1.0)      # will be 200% if 100% on 2 cores
        tmpCPUname = str(mesures.get("name") or "unknown")[:15]

        # true only if pattern specified and pattern found (a empty pattern matches everything)
        namecheck = ( len(namepattern) > 0 ) and re.search(pattern=namepattern, string=tmpCPUname, flags=re.IGNORECASE)

        if tmpCPUpc >= 0.0 and (tmpCPUpc > thresholdcpupercent or namecheck ) :
            if 'system idle' not in tmpCPUname.lower() :
                # --- Create influx measures from the process data
                Exep   = ""
                idbobj = copy.deepcopy(IDBTemplate)
                idbobj['time']           = agent_timezone.fromutc(datetime.datetime.utcnow()).isoformat()
                idbobj['measurement']    = 'processes'
                try :
                    # TAGS are indexed
                    tmpCPUname = tmpCPUname[:15]
                    idbobj['tags']['ProcessUID']  = "{:s}-{:d}".format(tmpCPUname, int(mesures.get("pid") or 0))
                    idbobj['tags']['ProcessName'] = "{:s}".format(tmpCPUname)
                except Exception as E:
                    # These data are mandatory, no push if we can't have them
                    Exep = "E1 " + str(E)
                else :
                    # FIELDS are not indexed
                    idbobj['fields'] = dict()
                    try :
                        idbobj['fields']['cpupercentpercore'] = tmpCPUpc  # DO NOT CALL AGAIN p.cpu_percent() because lib keep it in cache
                        idbobj['fields']['cpupercent']        = tmpCPUpc / nbcores
                        idbobj['fields']['memorypercent']     = float(mesures.get("memory_percent") or 0.0)
                    except Exception as E:
                        # No push if we don't have at least these infos
                        Exep = "E2 " + str(E)
                    else :
                        try :
                            tmpp = mesures.get("cpu_times") or None
                            if tmpp :
                                idbobj['fields']['cpuuser']    = float(tmpp.user)       # time spent on it
                                idbobj['fields']['cpusystem']  = float(tmpp.system)     # time spent on it
                            tmpp = mesures.get("memory_info") or None
                            if tmpp :
                                idbobj['fields']['memoryphys'] = int(tmpp.rss)
                                idbobj['fields']['memoryvirt'] = int(tmpp.vms)
                        except Exception as E4 :
                            Exep = "E3 " + str(E4)
                        else :
                            idbobj['fields']['nbthreads'] = int(mesures.get("num_threads") or int(0))
                            if psutil.WINDOWS :  # dispo uniquement sous windows
                                idbobj['fields']['nbhandles'] = int(mesures.get("num_handles") or int(0))

                if len(Exep) > 0 :
                    logging.error("Processes : Exception : %s" % str(Exep))
                    pass
                else :
                    retour4idb.append(idbobj)

    return retour4idb

# ----- Getting GPU metrics -----
def getGPUMetrics(IDBTemplate=None) :
    IDBTemplate  = dict() if not IDBTemplate  else IDBTemplate
    retour4idb   = list()
    global agent_timezone

    GPUs = GPUtil.getGPUs()
    for gpu in GPUs :
        idbobj = copy.deepcopy(IDBTemplate)
        idbobj['time']          = agent_timezone.fromutc(datetime.datetime.utcnow()).isoformat()
        idbobj['measurement']   = 'gpumetrics'
        idbobj['tags']['gpuID'] =  "{:s}-{:d}".format(getattr(gpu, 'name'), getattr(gpu, 'id'))
        idbobj['fields']        = dict()
        idbobj['fields']['tempDegC']    = float(getattr(gpu, 'temperature'))
        idbobj['fields']['loadPC']      = float(getattr(gpu, 'load'))*100
        idbobj['fields']['RAMusedPC']   = float(getattr(gpu, 'memoryUtil'))*100
        idbobj['fields']['RAMtotalMBy'] = float(getattr(gpu, 'memoryTotal'))
        idbobj['fields']['RAMusedMBy']  = float(getattr(gpu, 'memoryUsed'))
        idbobj['fields']['RAMfreeMBy']  = float(getattr(gpu, 'memoryFree'))
        retour4idb.append(idbobj)
    return retour4idb

if __name__ == '__main__':

    # ------------------------ INIT (exit -1 ou -2 if PB) ------------------------
    pyclasses.LogNConf.LogNConf.initLogging(level=logging.INFO, log_in_file=True)
    conf = pyclasses.LogNConf.LogNConf.getConf()
    logging.debug('Log/Conf Files : {:s}'.format(str(pyclasses.LogNConf.LogNConf.dumpPaths())))
    pyclasses.LogNConf.LogNConf.set_signals()

    # ------------------------ GLOBAL VAR (exit -3 if PB) ------------------------
    try :
        influxparams   = conf['influxdb']
        systemUID      = str(conf['identifiers']['systemUID']).replace(' ', '_')
        systemGrpUID   = str(conf['identifiers']['systemGrpUID']).replace(' ', '_')
        agent_timezone = pytz.timezone(conf.get('identifiers').get('timezone') or 'Europe/Paris')
        refreshSystem  = conf.get('system').get("refreshSeconds")    or 29
        refreshGPU     = conf.get('gpu').get("refreshSeconds")       or 29
        refreshProcess = conf.get('processes').get("refreshSeconds") or 59

        mqtt_enabled        = False if int(conf['mqttbroker']['enable']) == 0 else True
        mqttparams          = conf['mqttbroker']
        mqtt_dest_topic     = "{:s}/{:s}/{:s}".format(conf['mqttbroker']['topic_publish'], systemGrpUID, systemUID)
        mqttc               = None

        loop_main             = True
        loop_sending_works    = True
    except Exception as GAV :
        logging.critical("Exception in global var : {:s}".format(str(GAV)))
        sys.exit(-3)


    # ------------------------ LOOP_MAIN ------------------------
    while loop_main & pyclasses.LogNConf.LogNConf.lets_go_on :

        # ----------- Creation of IDB Client
        idbclient  = pyclasses.InfluxPush.InfluxPush(params=influxparams)
        kpi4idb    = list()

        # ----------- Object INFLUXDB : the template instance
        idbmesure = dict()
        idbmesure['time']   = agent_timezone.fromutc(datetime.datetime.utcnow()).isoformat() # will be overriden at the sensing TS
        idbmesure['fields'] = dict()
        idbmesure['tags']   = {'systemUID' : systemUID, 'systemGrpUID' : systemGrpUID  }

        # ----------- variables for the loops
        first_call_to_psutil_done   = False
        onepushdone                 = False
        loop_sending_works          = True
        PastMetrics                 = dict()
        PastMetrics['io-disks-ts']  = float(0.0)
        PastMetrics['io-disks-kpi'] = None
        PastMetrics['io-netw-ts']   = float(0.0)
        PastMetrics['io-netw-kpi']  = None
        ts_system = ts_gpu = ts_processes = float(0.0)
        erreur_log1 = erreur_log2 = erreur_log4 = False
        erreur_log3 = False

        # ----------- MQTT : Connection
        if mqtt_enabled :
            if not mqttc :
                mqttc = myMqttClient(mqttparams)
            else :
                mqttc.stop_and_disconnect()
            loop_sending_works = mqttc.init_connect_and_start()
            if not loop_sending_works :
                logging.error("Error connecting to MQTT, Sleeping 240s")
                pyclasses.LogNConf.LogNConf.sleep_with_signal_handling(sleeptime=240)

        # ----------- LOOPING
        logging.info("Starting monitoring (1st round won't be used)")
        while loop_sending_works & pyclasses.LogNConf.LogNConf.lets_go_on :
            time0 = time.perf_counter()

            # ---------- System monitoring
            if refreshSystem>0 and (time0 - ts_system >= refreshSystem) :
                try :
                    kpi4idb = kpi4idb + getSystemMetrics(PreviousData=PastMetrics, IDBTemplate=idbmesure)
                    ts_system = time.perf_counter()
                except Exception as E1 :
                    if not erreur_log1 :
                        logging.error("System Monitor {:s} : Exception = {:s}".format(str(datetime.datetime.isoformat(datetime.datetime.now())), str(E1)))
                        erreur_log1 = True
                else :
                    if erreur_log1 :
                        logging.error("System Monitor {:s} : Back to normal".format(str(datetime.datetime.isoformat(datetime.datetime.now()))))
                    erreur_log1 = False

            # ---------- GPU monitoring
            if refreshGPU>0 and (time0 - ts_gpu >= refreshGPU) :
                try :
                    kpi4idb = kpi4idb + getGPUMetrics(IDBTemplate=idbmesure)
                    ts_gpu  = time.perf_counter()
                except Exception as E1 :
                    if not erreur_log4 :
                        logging.error("GPU Monitor {:s} : Exception = {:s}".format(str(datetime.datetime.isoformat(datetime.datetime.now())), str(E1)))
                        erreur_log4 = True
                else :
                    if erreur_log4 :
                        logging.error("GPU Monitor {:s} : Back to normal".format(str(datetime.datetime.isoformat(datetime.datetime.now()))))
                    erreur_log4 = False

            # ---------- Process monitoring
            if refreshProcess>0 and (time.perf_counter() - ts_processes >= refreshProcess) :
                try :
                    kpi4idb = kpi4idb + getProcessMetrics(IDBTemplate=idbmesure)
                    ts_processes = time.perf_counter()
                except Exception as E2 :
                    if not erreur_log2 :
                        logging.error("Process Monitor {:s} : Exception = {:s}".format(str(datetime.datetime.isoformat(datetime.datetime.now())), str(E2)))
                        erreur_log2 = True
                else :
                    if erreur_log2 :
                        logging.error("Process Monitor {:s} : Back to normal".format(str(datetime.datetime.isoformat(datetime.datetime.now()))))
                    erreur_log2 = False

            # ---------- Sending to InfluxDB
            if first_call_to_psutil_done and len(kpi4idb) > 0 :
                if mqtt_enabled :
                    # ----------- Sending to MQTT
                    try :
                        mqtt_push_message = json.dumps(kpi4idb)
                    except Exception as ECP :
                        logging.error("Excep when dumping json : {:s}".format(str(ECP)))
                        mqtt_push_message = ""
                    else :
                        try :
                            msgInfo = mqttc.mqttclient.publish(topic=mqtt_dest_topic, payload=mqtt_push_message, qos=0, retain=False)
                            # msgInfo.wait_for_publish() # blocking in case of MQTT disconnection
                            logging.debug("Published on {:s} : {:s}".format(mqtt_dest_topic, str(mqtt_push_message)))
                            if not onepushdone:
                                onepushdone = True
                                logging.info("First push to MQTT sounds OK : {:d} items. No more logs except for errors...\n".format(len(kpi4idb)))
                        except Exception as EE :
                            logging.warning("Cannot publish on {:s} : {:s}".format(mqtt_dest_topic, str(EE)))
                            loop_sending_works = False   # a disconnect / reconnect will be done

                else :
                    # ----------- Sending direct to InfluxDB
                    try :
                        idbclient.insertInIDB(liste_objets=kpi4idb)
                        if not onepushdone:
                            onepushdone = True
                            logging.info("First push to IDB sounds OK : {:d} items. No more logs except for errors...\n".format(len(kpi4idb)))
                    except Exception as E3 :
                        loop_sending_works = False   # a disconnect / reconnect will be done
                        if erreur_log3 is True :
                            logging.error("InfluxDB Push {:s} : Exception = {:s}".format(str(datetime.datetime.isoformat(datetime.datetime.now())), str(E3)))
                            erreur_log3 = True
                    else :
                        if erreur_log3 :
                            logging.error("InfluxDB Push {:s} : Back to normal".format(str(datetime.datetime.isoformat(datetime.datetime.now()))))
                        erreur_log3 = False

            else :
                first_call_to_psutil_done = True

            # ---------- Sent : sleep between looping while connection is opened
            kpi4idb.clear()
            if loop_sending_works :
                time.sleep(3) # waiting a few seconds between loops, it is a timestep

        # ----------- MQTT : Deconnection between global loop
        if mqtt_enabled :
            mqttc.stop_and_disconnect()
        if loop_main & pyclasses.LogNConf.LogNConf.lets_go_on :
            # we are here because of errors, so waiting if the main loop is still OK.
            pyclasses.LogNConf.LogNConf.sleep_with_signal_handling(sleeptime=60)

    logging.info("Exiting")
    sys.exit(0)
